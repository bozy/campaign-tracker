require 'spec_helper'

describe RabbitmqService, rabbit: true do

  subject { described_class.new(rabbit: rabbit) }
  let!(:rabbit) { RSpec.configuration.rabbit }

  before(:each) do
    rabbit.start
    queue = rabbit.create_channel.queue('campaign_tracker_requests')
    queue.purge
  end

  after(:each) do
    rabbit.close
  end

  describe '#push' do
    it 'publishes messages' do
      executed = nil
      message = { a: 'message' }

      queue = rabbit.create_channel.queue('campaign_tracker_requests')
      queue.subscribe do |_delivery_info, _properties, body|
        executed = true
        expect(body).to be == message.to_json
      end
      subject.push(message)
      sleep(1)
      expect(executed).to be_truthy
    end
  end
end