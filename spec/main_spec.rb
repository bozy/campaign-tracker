require 'spec_helper'

describe Main do

  def app
    described_class
  end

  it 'for nonexisting slugs redirects to sharepop.com' do
    get '/notexists'
    expect(last_response.status).to be == 302
    expect(last_response.header['Location']).to be == 'http://sharepop.com'
  end

  it 'for existing slugs with active campaigns it redirects to the proper url' do
    get '/8765432capa'
    expect(last_response.status).to be == 302
    expect(last_response.header['Location']).to be == 'http://offer.url.com'
  end

  it 'for existing slugs with inactive campaigns it redirects to the sharepop.com' do
    get '/23458678tana'
    expect(last_response.status).to be == 302
    expect(last_response.header['Location']).to be == 'http://sharepop.com'
  end

  describe 'check queue', rabbit: true do

    let!(:rabbit) { RSpec.configuration.rabbit }

    before(:each) do
      rabbit.start
      queue = rabbit.create_channel.queue('campaign_tracker_requests')
      queue.purge
    end

    after(:each) do
      rabbit.close
    end

    it 'pushes request data to the queue' do
      caught_body = nil
      queue = rabbit.create_channel.queue('campaign_tracker_requests')
      queue.subscribe do |_delivery_info, _properties, body|
        caught_body = body
      end

      get '/23458678tana'
      sleep(1)
      expect(caught_body).to be == { headers: [['HTTP_HOST', 'example.org'], ['HTTP_COOKIE', '']],
                                     query_string: '' }.to_json
    end
  end
end