ENV['RACK_ENV'] = 'test'

require 'rack/test'
require 'yaml'
require 'mongo'
require_relative '../app/main'

Mongo::Logger.logger.level = ::Logger::FATAL

RSpec.configure do |config|
  config.include Rack::Test::Methods

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.before(:each, mongo: true) do
    mongo = Mongo::Client.new(['localhost:27017'], database: 'campaign_tracker_test')
    config.add_setting(:mongo, default: mongo)

    collection_names = mongo.database.collection_names.to_a
    collection_names.each do |collection_name|
      mongo[collection_name].find.delete_many
    end
  end

  # rabbit
  config.before(:each, rabbit: true) do
    rabbit = Bunny.new('amqp://localhost')
    config.add_setting(:rabbit, default: rabbit)
  end
end
