require 'spec_helper'

describe CacheService do

  subject { described_class.new({}) }

  class Echo
    def value(arg)
      arg
    end
  end

  describe '#fetch' do

    it 'calls the source only once' do
      echo = Echo.new
      expect(echo).to receive(:value).once.and_call_original

      # one
      expect(subject.fetch(:test) { |args| echo.value(args) }).to be == :test
      # two
      expect(subject.fetch(:test) { |args| echo.value(args) }).to be == :test
    end

    it 'recalls the source after expiration' do
      echo = Echo.new
      expect(echo).to receive(:value).exactly(2).times.and_call_original

      # one
      expect(subject.fetch(:test) { |args| echo.value(args) }).to be == :test

      # shift to the future
      now = Time.now
      allow(Time).to receive(:now) { now + described_class::DEFAULT_EXPIRATION_TIME + 1.minute }
      # two
      expect(subject.fetch(:test) { |args| echo.value(args) }).to be == :test
    end

  end
end
