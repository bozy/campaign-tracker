require 'spec_helper'
require_relative '../app/retryable_queue'

describe RetryableQueue do

  class TestQueue
    attr :logger
    include RetryableQueue

    def initialize
      @logger ||= Logger.new(STDOUT)
      super(executor: Concurrent::ImmediateExecutor.new)
    end
  end

  subject { TestQueue.new }

  describe '#push' do

    it 'calls "proceed" function' do
      expect(subject).to receive(:proceed).with(:a_message)

      subject.push(:a_message)
    end

    context 'when proceed raises an exception' do

      before do
        allow(subject).to receive(:proceed).and_raise(StandardError, 'Pushing failed')
      end

      it 'schedules a next execution' do
        expect(subject).to receive(:schedule_retry) do |message|
          expect(message[:retries]).to be == 0
          expect(message[:failed_at]).to be <= Time.now
          expect(message[:message]).to be == :a_message
        end

        subject.push(:a_message)
      end

      it 'each retry inc(retries)' do
        max_retries = 3
        retries_counter = 0
        expect(subject).to receive(:retry_push).exactly(max_retries).times.and_wrap_original do |m, message|
          expect(message[:retries]).to be == retries_counter
          expect(message[:failed_at]).to be <= Time.now
          expect(message[:message]).to be == :a_message
          retries_counter += 1
          if retries_counter < max_retries
            m.call(message)
          end
        end
        expect(Concurrent::ScheduledTask).to receive(:execute).exactly(max_retries).times.and_yield

        subject.push(:a_message)
      end

    end
  end
end