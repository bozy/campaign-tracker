require 'spec_helper'

describe RepoService, mongo: true do
  let!(:mongo) { RSpec.configuration.mongo }

  subject { described_class.new(mongo: mongo) }

  describe '#fetch' do

    before do
      campaigns_records = [
        { _id: 1,
          status: 'expired',
          offer_url: 'http://offer.url.com',
        }, {
          _id: 2,
          status: 'active',
          offer_url: 'http://offer.url.com',
        },

      ]
      short_links_records = [
        {
          _id: 1,
          campaign_id: 1,
          slug: '23458678tana',
        }, {
          _id: 2,
          campaign_id: 2,
          slug: '8765432capa',
        }]
      mongo[:campaigns].insert_many(campaigns_records)
      mongo[:short_links].insert_many(short_links_records)
    end

    it 'fetches offer_url by slug' do
      result = subject.fetch('8765432capa')
      expect(result).to be == 'http://offer.url.com'
    end

    it 'gets default url if the campaign is expired' do
      result = subject.fetch('23458678tana')
      expect(result).to be == 'http://sharepop.com'
    end

    it 'gets default url if the slug does not exist' do
      result = subject.fetch('unknown')
      expect(result).to be == 'http://sharepop.com'
    end

  end

  describe '#load_files' do

    it 'loads data to empty collections' do
      subject.load_files(campaigns: 'spec/artifacts/campaigns.json', short_links: 'spec/artifacts/shortlink.json')

      expect(mongo[:campaigns].find({}).map { |record| record['_id'] }).to be == [1, 2]
      expect(mongo[:short_links].find({}).map { |record| record['_id'] }).to be == [1, 2]
    end

    it 'raises proper exception if any file not found' do
      expect { subject.load_files(campaigns: '../artifacts/unknown.json') }.to raise_exception(Errno::ENOENT)
    end

    context 'if some records exists' do
      before do
        campaigns_records = [
          { _id: 1,
            status: 'expired',
            offer_url: 'http://shouldbethis.com',
          }]
        short_links_records = [
          {
            _id: 1,
            campaign_id: 1,
            slug: 'shouldbethis',
          }]
        mongo[:campaigns].insert_many(campaigns_records)
        mongo[:short_links].insert_many(short_links_records)
      end

      it 'loads only records with new ids' do
        subject.load_files(campaigns: 'spec/artifacts/campaigns.json', short_links: 'spec/artifacts/shortlink.json')

        expect(mongo[:campaigns].find(_id: 1).first['offer_url']).to be == 'http://shouldbethis.com'
        expect(mongo[:short_links].find(_id: 1).first['slug']).to be == 'shouldbethis'
      end
    end

  end
end