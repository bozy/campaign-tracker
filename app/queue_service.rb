require 'concurrent'
require_relative 'retryable_queue'

class QueueService
  attr_reader :logger

  include RetryableQueue

  def initialize(config)
    @messages = []
    @logger = config[:logger] || Logger.new(STDOUT)
    super
  end

  protected

  def proceed(message)
    @messages << message
    logger.info { "#{@messages.count} so far" }
    if rand(3) == 1
      logger.warn { 'Something bad is happening' }
      raise 'Something bad happened'
    end
  end
end
