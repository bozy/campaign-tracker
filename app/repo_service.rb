require 'json'

class RepoService
  attr_reader :short_links
  attr_reader :campaign
  attr_reader :logger
  attr_reader :mongo

  DEFAULT_URL = 'http://sharepop.com'

  def initialize(config)
    @mongo = config[:mongo]
    @short_links = @mongo['short_links']
    @campaign = @mongo['campaigns']
    @logger = config[:logger] || Logger.new(STDOUT)
  end

  def fetch(slug)
    record = short_links.find({ slug: slug }, { projecttion: { campaign_idq: 1 }, limit: 1 }).first
    return DEFAULT_URL unless record && record['campaign_id']

    record = campaign.find({ _id: record['campaign_id'], status: 'active' },
                           { projecttion: { offer_url: 1 }, limit: 1 }).first
    return DEFAULT_URL unless record && record['offer_url']

    record['offer_url']
  end

  def load_files(file_paths)
    # WARN File reading should be changed to Stream-like for huge files because it may cause Out of Memory

    file_paths.each do |collection, file_path|
      counter = 0
      records = parse_file(file_path)
      records.each do |record|
        record = transform(collection, record)
        unless record_exists?(collection, record)
          insert(collection, record)
          counter += 1
        end
      end
      logger.info { "Loaded #{counter} records into #{collection} collection" }
    end
  end

  protected

  def parse_file(file_path)
    file = File.read(file_path)
    JSON.parse(file)
  end

  def transform(collection, record)
    case collection
      when :short_links
        id = record.delete('shortlink_id')
        record.merge('_id' => id)
      else
        id = record.delete('id')
        record.merge('_id' => id)
    end
  end

  def record_exists?(collection, record)
    mongo[collection].find({ _id: record['_id'] }, { projection: { _id: 1 }, limit: 1 }).first
  end

  def insert(collection, record)
    mongo[collection].insert_one(record)
  end
end
