require_relative 'retryable_queue'

class RabbitmqService
  attr_reader :logger

  include RetryableQueue

  def initialize(config)
    @logger = config[:logger] || Logger.new(STDOUT)
    rabbit = config[:rabbit]
    rabbit.start
    @channel = rabbit.create_channel
    super
  end

  protected

  def proceed(message)
    payload = message.to_json
    queue = @channel.queue('campaign_tracker_requests')
    @channel.default_exchange.publish(payload, routing_key: queue.name)
  end
end
