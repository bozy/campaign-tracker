class CacheService
  DEFAULT_EXPIRATION_TIME = 10.minutes

  attr_reader :logger

  def initialize(config)
    @cache = {}
    @lifetime = config[:lifetime] || DEFAULT_EXPIRATION_TIME
    @logger = config[:logger] || Logger.new(STDOUT)
    logger.info { "Cache lifetime: #{@lifetime}s" }
  end

  def fetch(arg, &block)
    record = @cache[arg]
    record = put_record(arg, block) if !record || expired?(record)
    record[:value]
  end

  protected

  def put_record(arg, block)
    # WARN In real life cache should be cleaned up for the least uses records and should be set the max size to
    # prevent Out of Memory

    record = { put_at: Time.now, value: block.call(arg) }
    @cache[arg] = record
  end

  def expired?(cached)
    cached[:put_at] + @lifetime <= Time.now
  end
end
