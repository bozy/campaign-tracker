module RetryableQueue
  attr_reader :logger

  def initialize(config)
    @failed_messages_sequence = 0
    @logger = config[:logger] || Logger.new(STDOUT)
  end

  def push(message)
    proceed(message)
  rescue
    schedule_retry(failed_at: Time.now, retries: 0, message: message)
  end

  protected

  def proceed(*)
    logger.error { 'Implement proceed method' }
  end

  def retry_push(failure)
    logger.info { 'Retrying' }
    proceed(failure[:message])
  rescue
    failure = failure.merge(failed_at: Time.now, retries: failure[:retries] + 1)
    schedule_retry(failure)
  end

  def schedule_retry(failure)
    @failed_messages_sequence += 1
    delay = next_attempt_at(failure[:retries])
    logger.info { "Scheduling retry in #{delay}s" }
    Concurrent::ScheduledTask.execute(delay) { retry_push(failure) }
  end

  def next_attempt_at(retries)
    (2**retries) * 0.1
  end
end
