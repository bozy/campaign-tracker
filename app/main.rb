require 'sinatra'
require 'logger'
require 'sinatra/reloader'
require 'sinatra/custom_logger'
require 'sinatra/config_file'
require 'active_support/all'
require 'mongo'
require 'bunny'
require_relative 'queue_service'
require_relative 'rabbitmq_service'
require_relative 'repo_service'
require_relative 'cache_service'

class Main < Sinatra::Base
  helpers Sinatra::CustomLogger
  register Sinatra::ConfigFile
  register do
    def transfer_from_files
      Dir.chdir(root || '.') do
        settings.repo_service.load_files(settings.json_files.symbolize_keys)
      end
    rescue Errno::ENOENT => ex
      logger.warn { "Cannot load one of json files. Reason: #{ex}" }
    end
  end

  config_file '../config.yml'

  configure :development do
    enable :logging
    logger = Logger.new(STDOUT)
    logger.level = Logger::INFO
    set :logger, logger

    register Sinatra::Reloader
    also_reload 'app/**/*.rb'
  end

  configure :test do
    enable :logging
    logger = Logger.new(STDOUT)
    logger.level = Logger::INFO
    set :logger, logger
  end

  configure do
    Mongo::Logger.logger.level = ::Logger::FATAL
    set :queue_service,
        RabbitmqService.new(
          rabbit: ::Bunny.new(*settings.mq_args),
          logger: settings.logger
        )

    # Mock service simulating exceptions
    #
    # set :queue_service,
    #     QueueService.new(
    #       logger: settings.logger)

    set :repo_service,
        RepoService.new(
          mongo: Mongo::Client.new(*settings.mongo_args),
          logger: settings.logger
        )
    set :cache_service,
        CacheService.new(
          lifetime: settings.cache_lifetime.to_i.minutes,
          logger: settings.logger
        )
    transfer_from_files
  end

  get '/:slug' do |slug|
    url = fetch_url(slug)

    important_params = self.important_params
    queue_service.push(important_params)

    redirect url
  end

  protected

  def important_params
    headers = env.select { |k, _| k.start_with? 'HTTP_' }
                .map { |key, val| [key, val] }
    query_string = env['QUERY_STRING']
    { headers: headers, query_string: query_string }
  end

  def fetch_url(slug)
    cache_service.fetch(slug) { |arg| repo_service.fetch(arg) }
  end

  def queue_service
    settings.queue_service
  end

  def repo_service
    settings.repo_service
  end

  def cache_service
    settings.repo_service
  end
end
