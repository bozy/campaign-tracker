campaign-tracker
================
                                           
## Installation

### Clone the app
    $ git clone git@bitbucket.org:bozy/campaign-tracker.git
                                    
### RVM
    $ gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
    $ \curl -sSL https://get.rvm.io | bash -s stable
Details: https://rvm.io/

Proper ruby and gemset should be automagically installed after entering the directory.

    $ cd campaign-tracker

If it doesn't work ruby version and gemset are described in the .ruby-version and .ruby-gemset files.

### Bundler
    $ gem install bundler
    $ bundle install

### MongoDB and RabbitMQ
The application needs MongoDB and RabbitMQ. The connection strings are located in the `config.yml` and `spec/spec_helper.rb`.

## Test and running ##
    $ bundle exec rspec
    $ bundle exec thin start

### Configuration

The cache lifetime property and other configurable parameters are kept in the `config.yml` file.

**WARN:** To change connection strings for the RabbitMQ and MongoDB tests you need to to update `spec/spec__helper.rb` file too.

## Description

### Architecture
The application is based on the Sinatra framework. The "entry point" is located in `app/main.rb` at the `get '/:slug' do |slug|` line. The business logic is kept in:

* **RepoService** - DB access.
    * `fetch(slug)` - returns `offer_url` from the DB
    * `load_files(file_paths)` - loads json files into the DB
* **CacheService** - caches data in memory. Cache lifetime is configurable by `:lifiteme` config argument.
    * `fetch(arg, &block)` - runs `&block` with `arg` argument once and keeps the result in memory.Next call with the same `arg` returns the cached result.
* **RetryableQueue** - The mixin module  for MQ services. It requires the `proceed` method to be override.
    * `push(message)` - Tries to call "abstract" proceed method. If the call fails it schedules a task to make a next attempts in the future. Tasks are scheduled using `Cuncurrent::ScheduleTask.execute` method.
* **QueueService** - An example service including `RetryableQueue`. The `proceed` method fails with 25% chance.
* **RabbitmqService** - A more real life service including `RetryableQueue`. The proceed method uses RabbitMQ to push the message.